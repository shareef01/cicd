FROM openjdk
COPY ./target/simple-maven-app-1.0.jar /
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "/simple-maven-app-1.0.jar"]
